import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
    key:'calendar',
    storage:window.localStorage
});

export const store = new Vuex.Store({
    plugins:[vuexLocalStorage.plugin],
    state: {
        firstVisit:true,
        online:false,
        user:undefined,
        minPassLength:6,
    },
    getters: {
        firstVisit: state => { return state.firstVisit; },
        online: state => { return state.online; },
        user: state => { return state.user; },
        minPassLength: state => { return state.minPassLength; }
    },
    mutations: {
        setOnline: (state, payload) => {
            state.online = payload;
        },
        setUser: (state, payload) => {
            state.user = payload;
        },
        setFirstVisit: (state, payload) => {
            state.firstVisit = payload;
        }
    },
    actions: {
        setFirstVisit: ({ commit }, payload) => {
            commit('setFirstVisit', payload);
        },
        setUser: ({ commit }, payload) => {
            commit('setUser', payload);
        },
        setOnline: ({ commit, state }) => {
            Vue.http.get(process.env.SERVER_URL+'ping').then(ret => {
                // console.log('ping ret: ', ret);
                commit('setOnline', !!ret.body.status);

                if(!!ret.body.status) {
                    Vue.http.get(process.env.SERVER_URL+'isAuthenticated', {
                        headers:{ 'x-auth':state.user ? state.user.token : undefined }
                    }).then(ret => {
                        // console.log('isAuthenticated ret: ', ret);
                        if(ret.body.status === 'success') {
                            ret.body.user.token = ret.headers.get('x-auth');
                            commit('setUser', ret.body.user);
                        } else commit('setUser', null);
                    }, err => { commit('setUser', null); });
                } else commit('setUser', null);
            }, err => {
                commit('setUser', null);
                commit('setOnline', false);
            });
        }
    }
});
