import Vue from 'vue'
import Router from 'vue-router'

import calendar from '../components/route/Calendar.vue';
import home from '../components/route/Home.vue';
import register from '../components/route/user/Register.vue';
import login from '../components/route/user/Login.vue';
import activate from '../components/route/user/Activate.vue';
import notFound from '../components/route/static/NotFound.vue';
import account from '../components/route/user/Account.vue';
import passReset from '../components/route/user/PasswordReset.vue';
import docs from '../components/route/docs/Index.vue';

Vue.use(Router)

export default new Router({
  routes: [
    { path:'/', component:calendar },
    { path:'/home', component:home },
    { path:'/home/:id', component:home },
    { path:'/user/register', component:register },
    { path:'/user/login', component:login },
    { path:'/user/account', component:account },
    { path:'/user/activate/:id', component:activate },
    { path:'/user/resetPassword/:id', component:passReset },
    { path:'/docs', component:docs },
    { path:'/docs/:id', component:docs },
    { path:'*', component:notFound },
  ],
  mode:'history'
})
