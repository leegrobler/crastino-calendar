// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueResource from 'vue-resource';
import Snotify from 'vue-snotify';
import 'vue-snotify/styles/material.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faAngleUp, faSpinner, faAngleDown, faCheck, faTimes, faChevronLeft, faChevronRight, faAngleDoubleLeft, faAngleDoubleRight, 
  faPlus, faArrowLeft, faPen, faTrash, faCalendar, faBell, faSave, faClock, faRedoAlt, faHeart, faBalanceScale, faShareAlt, faUserPlus, 
  faBullhorn } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { store } from './store/store';

Vue.use(VueResource);
Vue.use(Snotify, {
  toast: {
    timeout:10000,
    showProgressBar:false,
    bodyMaxLength:200
  }
});

library.add(faAngleUp, faAngleDown, faSpinner, faCheck, faTimes, faChevronLeft, faChevronRight, faAngleDoubleLeft, faAngleDoubleRight,
  faPlus, faArrowLeft, faPen, faTrash, faCalendar, faBell, faSave, faClock, faRedoAlt, faHeart, faBalanceScale, faShareAlt, faUserPlus, 
  faBullhorn);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});

window.moment = require('moment');

export const eventBus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
