# Crastino

> Tomorrow in Latin  
>   
> A portfolio piece that is not in active use. Originally dubbed OrangeClockworx, this was the first application that I created using Vue.js on the front-end, Node.js on the back-end and MongoDB as the database (originally starting life as a Groovy/Grails project, shortly after the start of my professional career back in 2016!). The front-end was hosted on my Ubuntu Server VPS, but has since been moved to Firebase Hosting for preservation. It uses a REST API to communicate with the back-end and database that was hosted on Heroku.  
>   
> It is not currently operational, but the front-end is still online if you would like to get a feel for how this project worked.  

[Check it out](https://crastino-a0cb2.firebaseapp.com/)

## Run Locally

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

#### Author

Lee Grobler  
hello@lee-grobler.com  

### Technologies Used

 - HTML
 - CSS
 - Javascript
 - Vue
 - FontAwesome
 - Google Maps
